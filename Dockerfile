# Utiliser une image de base Python
FROM python:3.9

# Copier les fichiers de l'application dans le conteneur
COPY . /app

# Définir le répertoire de travail
WORKDIR /app

# Installer les dépendances Python
RUN pip install -r requirements.txt

# Commande à exécuter lorsque le conteneur démarre
CMD ["python", "ops.py"]
